# nodos

`nodos` helps you manage your GitLab Todos by marking as done those you are not
going to action. Currently `nodos` can close the following todos:
- Todos with group `mentions` or group `directly_addressed`
- Todos whose issue or MR has been closed or merged
- Todos for failed pipelines

## Usage
The `GITLAB_PAT` env var should contain a GitLab Personal Access Token(PAT)
with `api` scope.

```
$ bundle install
$ GITLAB_PAT=abcdefg ruby nodos.rb
Usage: nodos.rb <options>
    -g, --group                      Close group mentions or directly addressed
    -c, --closed                     Close if issue/MR has been closed or merged
    -b, --build                      Close if opened due to failed build
    -d, --dry-run                    Only print the todo ids that will be closed
    -h, --help                       Print this help
```

It is necessary to explicitly set the the flags for the closing criteria.

### Use 1Password to Store `GITLAB_PAT`

1Password has a [cool feature][1] that allows you to load your secrets directly
into an env var. To use this feature, you will need [1Password 8][2] and the 
[1Password command line utility][3].

Assuming your GitLab PAT is stored in `Private(Vault) -> GitLab Token(Item name)`
here are the steps to use 1Password with `nodos`.
- Store a reference to the access token in a `.gitlab-pat.env` file: 

```sh
# format is op://vault-name/item-name/[section-name/]field-name
$ echo "GITLAB_PAT=op://Private/GitLab Token/password" > $HOME/.gitlab-pat.env
```

- Define an alias in your shell profile to run `nodos` with the PAT

```sh
$ alias nodos="op run --env-file=$HOME/.gitlab-pat.env -- ruby /path/to/nodos.rb"
```

*Note*: Remember to source your shell profile, or create a new terminal session

- Run `nodos`

```sh
$ nodos -g -c -d
Current User: <username>
Total todos: 10
Total todos closed: 0
```

[1]: https://developer.1password.com/docs/cli/secrets-environment-variables 
[2]: https://1password.com/downloads/mac/
[3]: https://developer.1password.com/docs/cli
